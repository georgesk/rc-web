
all:

rsync_to_freeduc:
	rsync -av --delete --exclude="db*" --exclude="*archive*" --exclude="media" * freeduc.science:/srv/rc.freeduc.science/
	ssh freeduc.science "cd /srv/rc.freeduc.science; echo 'yes' | ./manage.py collectstatic; ./manage.py migrate"
	@echo "prochaines commandes : ssh freeduc.science, puis sudo service apache2 reload, et synchronisation de media/"
