---
title: une application web pour des repair-cafés
---

RC-web est une application web qui permet de gérer la communication
interne et externe des repair-cafés.

Rôles dans RC-web
=================

Quelques niveaux de rôles sont définis :

1. simple visiteur : pour voir les contenus tous-publics
2. bénévole : ajoute des informations pour les bénévoles, réparateurs et autres participants à l'accueil
3. secrétaire : peut organiser des flux d'information
4. administrateur : peut agir directement sur la base de données

Au-delà de simple visiteur, tous les rôles sont dépendants d'une authentification en ligne.

Fonctionnalités
===============

Cette liste va croissant.

 - déclarer un nouveau lieu où se tiendront des repair-cafés
 - définir des horaires habituels pour un lieu : par exemple 4ème jeudi du mois, 17 à 19 heures
 - créer de nouveaux évènements : pour un lieu, on déclare un ou plusieurs nouveaux évènements ; par défaut, les horaires habituels sont pris en compte
 

