# Generated by Django 4.2.11 on 2024-05-13 08:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gestion', '0009_rc_log_secretaire_confirme_reservation'),
    ]

    operations = [
        migrations.AddField(
            model_name='repair_cafe',
            name='bus',
            field=models.TextField(default='', verbose_name='Lignes de bus (format JSON)'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='repair_cafe',
            name='couleur',
            field=models.CharField(default='', max_length=10, verbose_name='Couleur pour le dépliant'),
            preserve_default=False,
        ),
    ]
