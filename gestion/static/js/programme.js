function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
	    const cookie = cookies[i].trim();
	    // Does this cookie string begin with the name we want?
	    if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
	    }
        }
    }
    return cookieValue;
}

function logout(href){
    location.replace("/logout/?next="+href);
}

function login(href){
    location.replace("/login/?next="+href);
}

function show_dialog(id, title){
    var elt=$("#"+id);
    elt.dialog({
	modal: true,
	title: title,
	minWidth: 350,
	minHeight: 200,
	show: { effect: "blind", duration: 800 },
    });
}

/**
 * Lance un dialogue modal en gérant un DIV temporaire
 * @param code le code HTML à utiliser dans le dialogue
 * @param title titre du dialogue
 * @param predefined dictionnaire champ => valeur
 * @param buttons tableau de boutons du dialogue
 **/
function make_dialog(code, title, predefined, buttons){
    var elt = $("<div>");
    $("body").append(elt);
    elt.html(code);
    /* mise en place des valeurs prédéfinies */
    for (var field in predefined) {
	elt.find("#"+field).val(predefined[field])
    }
    elt.dialog({
	modal: true,
	title: title,
	minWidth: 350,
	minHeight: 200,
	show: { effect: "blind", duration: 800 },
	close: function(event, ui) 
        { 
            $(this).dialog('destroy').remove();
	    $(document).remove(elt);
        },
	buttons: buttons || [],
    });
}

/**
 * Fait apparaître le dialogue de nouveau lieu
 **/
function nouveau_lieu(){
    $.post( "/lieuform", {
	csrfmiddlewaretoken: getCookie('csrftoken'),
    }).done(
	function(data){
	    make_dialog(
		data.html,
		"Nouveau lieu de Repair-Café",
		{
		    id_temp_id: -1,
		    csrfmiddlewaretoken: getCookie('csrftoken'),
		});
	}
    );
    
}

/**
 * Fait apparaître le dialogue d'édition de lieu
 * param elt le bouton qui cause l'édition
 **/
function edite_lieu(elt){
    var rc = $(elt).data("nom");
    $.post( "/lieuform", {
	csrfmiddlewaretoken: getCookie('csrftoken'),
	rc: rc,
	
    }).done(
	function(data){
	    make_dialog(
		data.html,
		"Édition du Repair-Café « "+rc+" »",
		{
		    id_temp_id: data.id,
		    submit_lieu: "Modifier « "+rc+" »",
		    csrfmiddlewaretoken: getCookie('csrftoken'),
		});
	}
    );
    
}

/**
 * Fait supprimer un lieu
 * param elt le bouton qui cause l'édition
 **/
function supprime_lieu(elt){
    var rc = $(elt).data("nom");
    $.post( "/del_lieuform", {
	csrfmiddlewaretoken: getCookie('csrftoken'),
	rc: rc,
	
    }).done(
	function(data){
	    make_dialog(
		data.html,
		"Suppression du Repair-Café « "+rc+" »",
		{
		    id_temp_id: data.id,
		    del_lieu: "Supprimer les enregistrements liés à « "+rc+" »",
		    csrfmiddlewaretoken: getCookie('csrftoken'),
		});
	}
    );
    
}

/**
 * fonction de rappel déclenchée par une demande de gestion d'horaires
 * @param elt l'élément bouton qui a déclenché l'appel
 **/
function horaires_lieu(elt){
    var rc = $(elt).data("nom");
    $.post( "/horaires_lieuform", {
	csrfmiddlewaretoken: getCookie('csrftoken'),
	rc: rc,
	
    }).done(
	function(data){
	    make_dialog(
		data.html,
		"Nouvelles dates du Repair-Café « "+rc+" »",
		{
		    id_temp_id: data.id,
		    submit_dates: "Créer de nouvelles dates pour « "+rc+" »",
		    csrfmiddlewaretoken: getCookie('csrftoken'),
		    id_date: data.date,
		    id_h_d: data.habitude.h_deb,
		    id_m_d: data.habitude.m_deb,
		    id_h_f: data.habitude.h_fin,
		    id_m_f: data.habitude.m_fin,
		    id_repetition: 1,
		});
	}
    );
    
}

/**
 * Affiche éventuellement un message s'il y a un div #message
 * @param duree facultatif; durée de l'affichage en ms ; 4 secondes par défaut
 **/
function affiche_message(duree){
    if (typeof(duree) == "undefined") duree=4000;
    var kind = "plain";
    var color="lightgreen";
    
    if ($("#message") && $("#message").text()){
	var akind = $("#message").data("kind");
	if (akind) kind = akind;
	if (kind == "warning") color="orange";
	if (kind == "error") color="pink";
	$("#message").dialog({
	    show: { effect: "blind", duration: 500 },
	    open: function(e) {
		$("#message").css('background-color',color);
	    },
	    position: { my: "right top", at: "right top", of: window },
	})
	setTimeout(function(){
	    $("#message").fadeOut({ effect: "blind", duration: 500 });
	    $("#message").dialog("close");
	}, duree
	);
    }
}

/**
 * Définition d'une plages d'heures habituelle pour un lieu
 **/
function heures_habituelles(elt){
    var nom = $(elt).data("nom");
    var form = $("#heures_habituelles");
    form.find("#id_lieu").val(nom);
    form.find("#id_nombre").val("1");
    show_dialog("heures_habituelles", "Heure d'ouverture habituelles de " + nom);
}

/**
 * affiche un évènement ; donne plus de possibilités aux secrétaires
 **/
function affiche_evenement(elt){
    elt = $(elt);
    var id = elt.data("id");
    var secretaire = elt.data("secretaire") == "True";
    $.post(
	"/affiche_evenement", {
	    csrfmiddlewaretoken: getCookie('csrftoken'),
	    id : id,
	    secretaire: secretaire,
	}
    ).done(function(data){
	make_dialog(data.html, data.lieu, {
	    csrfmiddlewaretoken: getCookie('csrftoken'),
	});
    });
}

/**
 * Gère la capture d'une image avec la webcam
 *
 * code fortement inspiré par
 * https://stackoverflow.com/questions/68130675/how-can-i-capture-a-picture-from-webcam-and-store-it-in-a-imagefield-or-filefiel/
 **/
function capture_photo(){
    var width = 320;    
    var height = 0;    
    var streaming = false;  
    var video = null;
    var canvas = null;
    var photo = null;
    var startbutton1 = null;
    
    function startup() {
	video = document.getElementById('video');
	canvas = document.getElementById('canvas');
	photo = document.getElementById('photo');
	startbutton = document.getElementById('startbutton');
	
	navigator.mediaDevices.getUserMedia({
	    /* video: true, */
	    video: {facingMode: {exact: "environment"}},
	    audio: false,
	}) .then(function(stream) {
	    video.srcObject = stream;
	    video.play();
	})
	    .catch(function(err) {
		var msg = "An error occurred: " + err;
		console.log("An error occurred: " + err);
		if (msg.indexOf("OverconstrainedError") >= 0){
		    navigator.mediaDevices.getUserMedia({
			video: true,
			audio: false,
		    }) .then(function(stream) {
			video.srcObject = stream;
			video.play();
		    })
		}
	    });
	
	video.addEventListener('canplay', function(ev){
            if (!streaming) {
		height = video.videoHeight / (video.videoWidth/width);
		
		
		if (isNaN(height)) {
		    height = width / (4/3);
		}
		
		video.setAttribute('width', width);
		video.setAttribute('height', height);
		canvas.setAttribute('width', width);
		canvas.setAttribute('height', height);
		streaming = true;
            }
	}, false);
	
	startbutton.addEventListener('click', function(ev){
            takepicture();
            ev.preventDefault();
	}, false);
	
	clearphoto();
    }
    
    function clearphoto() {
	var context = canvas.getContext('2d');
	context.fillStyle = "#AAA";
	context.fillRect(0, 0, canvas.width, canvas.height);
	
	var data = canvas.toDataURL('image/png');
	photo.setAttribute('src', data);
    }
  
    function takepicture() {
	var context = canvas.getContext('2d');
	if (width && height) {
            canvas.width = width;
            canvas.height = height;
            context.drawImage(video, 0, 0, width, height);

	    $("#camera").hide();
	    $("#captured").show();
            var data = canvas.toDataURL('image/png');
            photo.setAttribute('src', data);
	    $("#webimg").val(data);
	} else {
            clearphoto();
	}
    }
    window.addEventListener('load', startup, false);
}

$( document ).ready(function(){
    affiche_message();
});

/**
 * Cache la photo, montre à nouveau la vidéo
 **/
function reset_video(){
    $('#camera').show();
    $('#captured').hide();
}

/**
 * Demande confirmation, puis lie une demande de réparation à un évènement
 * de Repair-Café
 * @param elt le bouton qui a été cliqué, dans la page /rdv (en mode ticket)
 **/
function choisit_evenement(elt){
    elt=$(elt);
    $.post(
	"/choisit_evenement", {
	    ev: elt.data("ev"),
	    ticket:  elt.data("ticket"),
	    csrfmiddlewaretoken: getCookie('csrftoken'),
	},
    ).done(
	function(data){
	    make_dialog(
		data.html,
		"Confirmer le choix de Repair-Café",
		{
		    id_temp_id: -1,
		    csrfmiddlewaretoken: getCookie('csrftoken'),
		});
	}
    );
}

/**
 * Dialogue pour qu'un.e secrétaire s'occupe d'une demande de réparation
 * @param elt l'élément bouton déclencheur
 **/
function secretaire_pour_ticket(elt){
    var ticket = $(elt).data("ticket");
    $.post("secretaire_pour_ticket", {
	ticket: ticket,
	csrfmiddlewaretoken: getCookie('csrftoken'),
    }). done( function(data){
	var dialog = $("#dialog");
	dialog.html(data.html);
	dialog.dialog({
	    buttons: [
		{
		    text: "Échap.",
		    click: function() {
			$( this ).dialog( "close" );
		    }
		},
		{
		    text: "Ok",
		    icon: "ui-icon-heart",
		    click: function() {
			var csrf = $("<input name='csrfmiddlewaretoken' value='"+getCookie('csrftoken')+"'>")
			$("#form").append(csrf);
			$("#form").trigger("submit");
			$( this ).dialog( "close" );
		    }
		},
	    ],
	    modal: true,
	})
    });
}

/**
 * modification de l'évènement rendez-vous, dans la page
 * secretaire_modif_ticket
 **/
function modifie_evenement(elt){
    elt=$(elt);
    var ev = elt.data("ev");
    $.post("/quel_evenement", {
	id: ev,
	csrfmiddlewaretoken: getCookie('csrftoken'),
    }).done(function(data){
	$("#id_evenement").val(data.val);
	$("#id_id_event").val(data.id);
    });
}

/**
 * Fait apparaître le dialogue de choix de lieu
 * pour un.e secrétaire
 **/
function change_session_lieu(){
    $.post( "/change_session_lieu", {
	csrfmiddlewaretoken: getCookie('csrftoken'),
	lieu: $("#default_lieu").text(),
    }).done(
	function(data){
	    make_dialog(
		data.html,
		"Choix du lieu par défaut",
		{
		    csrfmiddlewaretoken: getCookie('csrftoken'),
		},
		[ // les boutons
		    {
			text: "Échap.",
			click: function() {
			    $( this ).dialog( "close" );
			}
		    },
		    {
			text: "Ok",
			icon: "ui-icon-heart",
			click: function() {
			    $.post(
				"/change_session_lieu_go",
				{
				    csrfmiddlewaretoken: getCookie('csrftoken'),
				    lieu: $("#lieu").val(),
				}
			    ). done(
				function(data){
				    if (data.OK == "true"){
					console.log("GRRRR reloading");
					location.reload();
				    }
				}
			    );
			    $( this ).dialog( "close" );
			}
		    },
		]
	    );
	}
    );
    
}

/**
 * Fait apparaître le dialogue de choix de date de travail
 * pour un.e secrétaire
 **/
function change_session_date_travail(){
    var date1 = $("#default_date").text()
    $.post( "/change_session_date_travail", {
	csrfmiddlewaretoken: getCookie('csrftoken'),
	lieu: $("#default_lieu").text(),
	date1: date1,
	date2: $("#default_date2").text() || date1,
    }).done(
	function(data){
	    make_dialog(
		data.html,
		"Choix de la date de travail",
		{
		    csrfmiddlewaretoken: getCookie('csrftoken'),
		},
		[ // les boutons
		    {
			text: "Échap.",
			click: function() {
			    $( this ).dialog( "close" );
			}
		    },
		    {
			text: "Ok",
			icon: "ui-icon-heart",
			click: function() {
			    $.post(
				"/change_session_date_travail_go",
				{
				    csrfmiddlewaretoken: getCookie('csrftoken'),
				    date1: $("#date1").val(),
				    date2: $("#date2").val(),
				}
			    ). done(
				function(data){
				    if (data.OK == "true"){
					location.reload();
				    }
				}
			    );
			    $( this ).dialog( "close" );
			}
		    },
		]
	    );
	}
    );
    
}

/**
 * Ajout d'une nouvelle catégorie, dans la page '/categories'
 **/
function nouvelle_categorie(){
    make_dialog(
	"<div>N'oubliez pas de modifier la catégorie après en avoir défini le nom, pour donner des exemples et définir un logo</div><div><label for='nom'>Nom de la catégorie :</label> <input type='text' name='nom' id='nom'/></div>",
	"Nouvelle catégorie",
	{
	    csrfmiddlewaretoken: getCookie('csrftoken'),
	},
	[ // les boutons
	    {
		text: "Échap.",
		click: function() {
		    $( this ).dialog( "close" );
		}
	    },
	    {
		text: "Ok",
		icon: "ui-icon-heart",
		click: function() {
		    $.post(
			"/nouvelle_categorie",
			{
			    csrfmiddlewaretoken: getCookie('csrftoken'),
			    nom: $("#nom").val(),
			}
		    ). done(
			function(data){
			    if (data.OK == "true"){
				location.reload();
			    }
			}
		    );
		    $( this ).dialog( "close" );
		}
	    },
	]
    )	
}


/**
 * Suppression d'une catégorie, à partir de la page /categories
 * @param elt le bouton qui a été pressé
 **/
function supprime_categorie(elt){
    var nom = $(elt).data("nom")
    make_dialog(
	"<div>Voulez-vous vraiment supprimer la catégorie " + nom + " ?</div>",
	"Nouvelle catégorie",
	{
	    csrfmiddlewaretoken: getCookie('csrftoken'),
	},
	[ // les boutons
	    {
		text: "Échap.",
		click: function() {
		    $( this ).dialog( "close" );
		}
	    },
	    {
		text: "Ok",
		icon: "ui-icon-heart",
		click: function() {
		    $.post(
			"/supprime_categorie",
			{
			    csrfmiddlewaretoken: getCookie('csrftoken'),
			    nom: nom,
			}
		    ). done(
			function(data){
			    if (data.OK == "true"){
				location.reload();
			    }
			}
		    );
		    $( this ).dialog( "close" );
		}
	    },
	]
    )	
    
}

/**
 * Rajoute une catégorie à un lieu
 * @param elt l'élément select qui vient de changer
 **/
function lieu_plus_categorie(elt){
    elt=$(elt)
    var lieu_id = elt.data("lieu")
    var categorie_id = elt.val()
    elt.val("")
    $.post(
	"/lieu_plus_categorie",
	{
	    csrfmiddlewaretoken: getCookie('csrftoken'),
	    lieu_id: lieu_id,
	    categorie_id: categorie_id,
	},
    ).done(
	function(data){
	    if(data.OK == "true") {
		location.reload()
	    }
	}
    )
}

/**
 * Supprime une relation entre catégorie et lieu
 * @param elt l'élément bouton qui a été cliqué
 **/
function lieu_moins_categorie(elt){
    elt=$(elt)
    $.post(
	"/lieu_moins_categorie",
	{
	    csrfmiddlewaretoken: getCookie('csrftoken'),
	    lieu_id: elt.data("lieu"),
	    categorie_id: elt.data("categorie"),
	},
    ).done(
	function(data){
	    if(data.OK == "true") {
		location.reload()
	    }
	}
    )
}

/**
 * Explique une catégorie, dans un dialogue.
 **/
function explique_categorie(elt){
    elt=$(elt)
    var nom = elt.data("nom")
    var exemples = elt.data("exemples")
    var src = elt.attr("src")
    var html = `<h2>Catégorie de réparations : ${nom}</h2>
<p>Dans cette catégorie, voici quelques exemples d'objets : ${exemples}</p>
<img src="${src}" alt="logo de ${nom}">`
    make_dialog(
	html,
	"Explication des catégories",
	{},
	[ // les boutons
	    {
		text: "OK",
		click: function() {
		    $( this ).dialog( "close" );
		}
	    },
	]

    )
}
