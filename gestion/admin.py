from django.contrib import admin

from .models import *

# Register your models here.
admin.site.register(Repair_Cafe)
admin.site.register(Evenement)
admin.site.register(Benevole)
admin.site.register(Secretaire)
admin.site.register(Participation_Benevole)
admin.site.register(Objet_Reparable)
admin.site.register(Reserve_Reparation)
admin.site.register(Qui_Repare)
admin.site.register(Reparation_Faite)
admin.site.register(RC_Log)
admin.site.register(Confirme_Reservation)
admin.site.register(LC)
admin.site.register(Categorie)
