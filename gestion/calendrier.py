"""
De quoi fabriquer un calendrier imprimable
"""
__all__ = ['calendrierImprimable', 'Table_Mois', 'Couleur_Du_Jour',
           'hex_to_rgb', 'hex_to_yiq']

from reportlab.lib.units import cm, mm
from reportlab.lib import colors
from reportlab.platypus import Table, TableStyle
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus.flowables import Image
from reportlab.lib.pagesizes import A4, A5
from reportlab.platypus import SimpleDocTemplate

#from reportlab.graphics.shapes import Drawing

from io import BytesIO
import PIL
from PIL import ImageFont, ImageDraw # pas Image, réservé à flowables
import datetime
import calendar
from colorsys import rgb_to_yiq


pdfmetrics.registerFont(TTFont('Vera', 'Vera.ttf'))
pdfmetrics.registerFont(TTFont('VeraBd', 'VeraBd.ttf'))

def hex_to_rgb(c):
    """
    convertit le format "#rrvvbb" en un triplet (r, g b)
    """
    return (int(c[1:3],16), int(c[3:5],16), int(c[5:7],16))

def hex_to_yiq(c):
    """
    convertit le format "#rrvvbb" en un triplet (Y, I, Q)
    """
    return rgb_to_yiq(int(c[1:3],16), int(c[3:5],16), int(c[5:7],16))

class Couleur_Du_Jour:
    """
    De quoi gérer la couleur d'un jour donné
    Paramètres du constructeur :

    @param jour le numéro du jour
    @param *couleurs une liste de couleurs (au moins une au format "#rrggbb"
    """

    def __init__(self, jour, *couleurs):
        self.jour = jour
        self.couleurs = couleurs
        return

    def __str__(self):
        return f"Couleur_Du_Jour : jour = {self.jour}, couleurs = {self.couleurs}"
    
    @property
    def rgb_list(self):
        """
        Donne une liste de couleurs au format rgb
        """
        return [hex_to_rgb(c) for c in self.couleurs]
    
class Table_Mois(Table):
    """
    un flowable qui représente un mois pour le calendrier des repair-cafés
    Paramètres du constructeur :
    @param annee l'année
    @param mois  le mois
    @param couleurs une liste d'instances de Couleur_Du_Jour
           (liste vide par défaut)
    """

    les_mois = ["JANVIER", "FÉVRIER", "MARS",
                "AVRIL", "MAI", "JUIN",
                "JUILLET", "AOÛT", "SEPTEMBRE",
                "OCTOBRE", "NOVEMBRE", "DÉCEMBRE"]
    
    def __init__(self, annee, mois, couleurs=[]):
        #########################################
        # création d'une matrice pour le tableau
        #########################################
        # première ligne : les noms de jours
        self.cal = [['L', 'M', 'M', 'J', 'V', 'S', 'D']]
        # ajout de lignes avec les numéros des jours
        self.cal.extend(calendar.monthcalendar(annee,mois))
        
        # on retient le nombre de dimanches du mois, pour les griser
        self.dimanches = len([l for l in self.cal[1:] if l[6] != 0])

        # premier numéro de semaine
        self.premiereseimaine = datetime.date(annee, mois, 1).isocalendar()[1]
        
        # peaufinage de la matrice
        for i,l in enumerate(self.cal):
            for j,n in enumerate(l):
                if n == 0:
                    self.cal[i][j] = ""   # met du vide pour jour = 0
            # ajout de la colonnes des semaines, au début
            if i == 0:
                sem = "Sem."                              # première ligne
            else:
                sem = f"{self.premiereseimaine+i-1:02d}"  # lignes suivantes
            self.cal[i].insert(0, sem)
            
        # prise en compte des couleurs ; en fait quand il faut mettre une
        # couleur de fond, on fait un flowable avec un fond coloré et du texte
        # qu'on met à la place du texte initial
        font = ImageFont.truetype("VeraBd.ttf", 80)
        for c in couleurs:
            for i in range(1, len(self.cal)):
                for j in range(1,7):
                    if self.cal[i][j] == c.jour: # c'est le bon jour à colorer
                        image = PIL.Image.new("RGB", (240,180), c.couleurs[0])
                       
                        draw = ImageDraw.Draw(image)
                        # traitements de couleurs suplémentaires
                        if len(c.couleurs) > 1:
                            draw.line([(0, 179), (239, 0)],
                                      fill = c.couleurs[1])
                            ImageDraw.floodfill(image,(130,100),
                                                hex_to_rgb(c.couleurs[1]))
                        fill = (255,255,255)
                        luma = hex_to_yiq(c.couleurs[0])[0]
                        if luma > 135:
                            #le fond est clair
                            fill = (0,0,0)
                        draw.text((70, 40), f"{c.jour:2d}", font=font, fill=fill)
                        imgBytes = BytesIO()
                        image.save(imgBytes, 'PNG')
                        self.cal[i][j] = Image(imgBytes, 20,20)

        # la matrice self.cal étant créée, on peut initialiser
        # la table intérieure
        self.table_interieure = Table(
            self.cal, 7*[8*mm], len(self.cal) * [6.0*mm])
        caption = f"{self.les_mois[mois-1]} {annee}"
        # on a tout pour initialiser
        Table.__init__(self, [[caption],[self.table_interieure]],
                       [6.6*cm], [1*cm, 4.35*cm])
        # style de la table complète
        self.setStyle(TableStyle([
            ('FONT', (0, 0), (-1, 0), 'VeraBd'),
            ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
            ('TEXTCOLOR', (0, 0), (0, -1), colors.white),
            ('BACKGROUND', (0, 0), (-1, -1), colors.navy),
        ]))
                      
        # On définit un style pour la table intérieure
        self.style = TableStyle([
            ('FONT', (0, 0), (-1, -1), 'Vera'),
            ('FONT', (1, 0), (-1, 0), 'VeraBd'),
            ('FONTSIZE', (0, 0), (-1, -1), 8),
            ('TEXTCOLOR', (0, 0), (0, -1), colors.grey),
            ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
            ('BACKGROUND', (0, 0), (-1, -1), colors.white),
        ])
        premier_dimanche = (-1,  1)
        dernier_dimanche = (-1, self.dimanches - len(self.cal))
        self.style.add('BACKGROUND', premier_dimanche, dernier_dimanche, colors.lightgrey)
        self.table_interieure.setStyle(self.style)
        return

def plusieurs_mois(*table_mois):
    """
    Réalise un flowable de plusieurs mois, qui sont placés dans deux colonnes
    """
    table_mois = list(table_mois)
    nombre = len(table_mois);
    lignes = (nombre + 1) // 2
    colonnes = 2

    if nombre % 2 == 1:
        # on ajoute un vide à la fin
        table_mois.append("")

    t=[]
    for l in range(lignes):
        t.append([])
        for c in range(colonnes):
            t[-1].append(table_mois[2*l + c])
    
    table = Table(t, 2*[7.4*cm], lignes * [5.8*cm])
    table.setStyle(TableStyle([
            ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
            ('VALIGN', (0, 0), (-1, -1), 'MIDDLE'),
        ]))
    return table

def calendrierImprimable(*table_mois, pagesize = A5, showBoundary = 0):
    """
    Place plusieurs mois de calendrier dans un document
    @return un BytesIO
    """
    buffer = BytesIO()
    doc = SimpleDocTemplate(
        buffer, pagesize=A5,
        topMargin = 0.7*cm, bottomMargin = 0.7*cm,
        leftMargin = 3.5*mm, rightMargin = 8*mm,
        topPadding = 0, bottomPadding = 0,
        leftPadding = 0, rightPadding = 0,
        showBoundary = showBoundary,
    )
    pm = plusieurs_mois(*table_mois)
    doc.build([pm])
    buffer.seek(0)
    return buffer

if __name__ == "__main__":
    """
    doc = SimpleDocTemplate(
        '/tmp/calendar.pdf', pagesize=A5,
        topMargin = 0.7*cm, bottomMargin = 0.7*cm,
        leftMargin = 0, rightMargin = 0,
        leftPadding = 3.5*mm, rightPadding = 8*mm,
        showBoundary=0,
    )
    pm = plusieurs_mois(
        Table_Mois(2024, 1),
        Table_Mois(2024, 2),
        Table_Mois(2024, 3),
        Table_Mois(2024, 4),
        Table_Mois(2024, 5, couleurs= [
            Couleur_Du_Jour(10, "#7777ff"), # le 10 de ce mois, fond cyan
            Couleur_Du_Jour(17, "#ffee77", "#ff0000"), # le 17 de ce mois, fond jaune et rouge
        ]),
    )
    doc.build([pm])
    """
    ci = calendrierImprimable(
        Table_Mois(2024, 1),
        Table_Mois(2024, 2),
        Table_Mois(2024, 3),
        Table_Mois(2024, 4),
        Table_Mois(2024, 5, couleurs= [
            Couleur_Du_Jour(10, "#7777ff"), # le 10 de ce mois, fond cyan
            Couleur_Du_Jour(17, "#ffee77", "#ff0000"), # le 17 de ce mois, fond jaune et rouge
        ])
    )
    with open("/tmp/calendar.pf", "wb") as outfile:
        outfile.write(ci.read())
    print("written /tmp/calendar.pf")
