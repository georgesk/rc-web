from django.forms import ModelForm, Form, PasswordInput, \
   HiddenInput, ValidationError

from django import forms
from django.forms import ModelForm
from django.core.exceptions import ValidationError
from PIL import Image

from .models import *

import re

class donnees_utilisateur_Form(Form):
   nom = forms.CharField(label="Nom", max_length=25, required = False)
   prenom = forms.CharField(label="Prénom", max_length=25, required = False)
   email = forms.CharField(label="Courriel", max_length=50, required = False)
   passe1 = forms.CharField(label="Mot de passe", max_length=32,
                            widget=PasswordInput, required = False)
   passe2 = forms.CharField(label="Répétez le mot de passe", max_length=32,
                            widget=PasswordInput, required = False)
   next = forms.CharField(max_length=50, widget = HiddenInput)

   def __init__(self, *args, **kwargs):
      super(donnees_utilisateur_Form, self).__init__(*args, **kwargs)
      self.fields['nom'].widget.attrs['placeholder'] = 'Votre nom ?'
      self.fields['prenom'].widget.attrs['placeholder'] = 'Votre prénom ?'
      self.fields['email'].widget.attrs['placeholder'] = 'Votre courriel ?'
      self.fields['passe1'].widget.attrs['placeholder'] = \
         'Nouveau mot de passe éventuel'
      self.fields['passe2'].widget.attrs['placeholder'] = 'Le même ...'
      return
     
   def clean_passe2(self):
      p1 = self.cleaned_data["passe1"]
      p2 = self.cleaned_data["passe2"]
      if p1 and not p2 == p1:
         raise ValidationError(
            "Erreur : les deux mots de passes étaient différents, recommencez")
      if p1 and len(p1) < 7:
         raise ValidationError(
            "Erreur : un mot de passe doit contenir au moins 7 caractères")
      minuscules = re.findall(r"[a-z]", p1)
      majuscules = re.findall(r"[A-Z]", p1)
      chiffres = re.findall(r"\d", p1)
      if p1 and (not minuscules or not majuscules or not chiffres):
          raise ValidationError(
            "Erreur : un mot de passe doit contenir au moins une lettre minuscule, une majuscule, et un chiffre")
      return p2


class RepairCafeForm(ModelForm):

   temp_id = forms.IntegerField(widget=forms.HiddenInput)
   
   class Meta:
      model = Repair_Cafe
      fields = (
         'nom_court',
         'adresse',
         'tel_contact',
         'mel_contact',
         'nb_tables',
         'nb_prises',
         'nb_personnes',
         'surperficie',
         'jour',
         'semaine',
         'h_deb',
         'm_deb',
         'h_fin',
         'm_fin',
         'couleur',
         'bus',
      )

      def clean(self):
        cleaned_data = super().clean()
        duree = 60*int(cleaned_data.get("h_f")) + \
            15*int(cleaned_data.get("m_f")) - \
            60*int(cleaned_data.get("h_d")) - \
            15*int(cleaned_data.get("m_d"))
        if duree < 60:
            raise ValidationError(f"durée trop courte : {duree} minutes")
        return cleaned_data

class Del_Lieu_Form(Form):
   temp_id = forms.IntegerField(widget=forms.HiddenInput)
   lieu = forms.CharField(label="Nom", max_length=25, required = False)

class Horaire_LieuForm(Form):
   
   temp_id = forms.IntegerField(widget=forms.HiddenInput)
   date = forms.DateField(label="Première date")
   h_d = forms.ChoiceField(choices=Horaire.HEURES, label="Heure de début")
   m_d = forms.ChoiceField(choices=Horaire.MINUTES, label="Minute de début")
   h_f = forms.ChoiceField(choices=Horaire.HEURES, label="Heure de fin")
   m_f = forms.ChoiceField(choices=Horaire.MINUTES, label="Minute de fin")
   repetition = forms.IntegerField(label = "Nombre de répétitions")
   
class LoginForm(ModelForm):
   passw = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)
   class Meta:
        model = User
        fields = (
           "username",
           "passw",
        )
   
class DemandeReparationForm(Form):
    nom = forms.CharField(
       max_length=50, label="Désignation de l'objet à réparer")
    poids = forms.FloatField(
       label="Son poids en kilos (environ)")
    panne = forms.CharField(label = "Description de la panne, en quelques mots",
                            widget=forms.Textarea)
    demandeur_nom = forms.CharField(label = "Mon nom")
    demandeur_prenom = forms.CharField(label = "Mon prénom")
    tel = forms.CharField(label = "N° de téléphone")
    mel = forms.CharField(label = "Adresse de courriel")
    
    def __init__(self, *args, **kwargs):
        super(DemandeReparationForm, self).__init__(*args, **kwargs)
        def attributes(fname, **kw):
           for k, v in kw.items():
              self.fields[fname].widget.attrs[k] = v
           return
        attributes('nom',  placeholder = 'Par exemple : grille-pain')
        attributes('poids', placeholder = 'Par exemple : 0,5')
        attributes(
           'panne',
           placeholder = 'Par exemple : s\'arrête de fonctionner après quelques secondes',
           rows = "3", cols = "21",
        )
        attributes(
           'demandeur_nom',
           placeholder = 'NOM'
        )
        attributes(
           'demandeur_prenom',
           placeholder = 'Prénom'
        )
        attributes(
           'tel',
           placeholder = 'Par ex. 06 05 04 03 02',
        )
        attributes(
           'mel',
           placeholder = 'prenom.nom@exemple.com',
        )
        return
     
class TicketForm(Form):
   ticket = forms.CharField(label="Numéro du ticket")

   def __init__(self, *args, **kwargs):
        super(TicketForm, self).__init__(*args, **kwargs)
        self.fields['ticket'].widget.attrs['placeholder'] = \
           'Par exemple : 12ab34'

class ModifReparationForm(Form):
    nom = forms.CharField(max_length=50, label="Désignation")
    poids = forms.FloatField(label="Poids")
    panne = forms.CharField(label = "Panne", widget=forms.Textarea)
    demandeur_nom = forms.CharField(label = "Nom")
    demandeur_prenom = forms.CharField(label = "Prénom")
    numero_tel = forms.CharField(label = "Téléphone")
    email = forms.CharField(label = "Courriel")
    evenement = forms.CharField(label="Où et quand", widget=forms.Textarea)
    id_event = forms.CharField(widget=forms.HiddenInput)
    
    def __init__(self, *args, **kwargs):
        super(ModifReparationForm, self).__init__(*args, **kwargs)
        def attributes(fname, **kw):
           for k, v in kw.items():
              self.fields[fname].widget.attrs[k] = v
           return
        attributes(
           'panne',
           rows = "2", cols = "21",
        )
        attributes(
           'evenement',
           rows = "2", cols = "21",
        )
        return
     
class AdopteReparationForm(Form):
   commentaire = forms.CharField(label = "Commentaire", widget=forms.Textarea)

   def __init__(self, *args, **kwargs):
        super(AdopteReparationForm, self).__init__(*args, **kwargs)
        def attributes(fname, **kw):
           for k, v in kw.items():
              self.fields[fname].widget.attrs[k] = v
           return
        attributes(
           'commentaire',
           rows = "2", cols = "21",
        )
        return
     
class categorie_Form(ModelForm):
   class Meta:
      model = Categorie
      fields = ["nom", "exemples", "logo"]
   
   def __init__(self, *args, **kwargs):
        super(categorie_Form, self).__init__(*args, **kwargs)
        def attributes(fname, **kw):
           for k, v in kw.items():
              self.fields[fname].widget.attrs[k] = v
           return
        attributes('nom', readonly= '1')
        attributes('nom', title= 'non modifiable ici')
        attributes(
           'exemples',
           placeholder = 'Par exemple : cafetières, théières, service à patisserie ...',
           rows = "3", cols = "21",
        )
        return
     
   def clean_logo(self):
     logo = self.cleaned_data.get('logo', False)
     if logo:
        format = Image.open(logo.file).format
        logo.file.seek(0)
        if format in ('PNG', 'JPEG'):
           return logo
        raise forms.ValidationError('type de fichier non autorisé : seules les formats jpeg et png sont valides ici.')
     return
