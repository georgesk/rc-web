from django.db import models
from django.db.models import *
from django.utils import timezone, safestring
from django.contrib.auth.models import User, Group
from django.utils.safestring import mark_safe
from django.contrib.auth.models import AnonymousUser
import sys, time, re, json, os
from datetime import timedelta, date, datetime, tzinfo
import pytz
import hashlib
import unidecode

from repaircafe.settings import TIME_ZONE, MEDIA_ROOT
from .calendrier import hex_to_yiq

# Create your models here.

class Lieu(models.Model):
    """
    Lieu pour un repair-café (classe abstraite)
    """
    class Meta:
       abstract = True
       
    nom_court = CharField(max_length=50, verbose_name = "Nom court" )
    adresse = TextField()
    tel_contact = CharField(max_length=25, verbose_name = "Téléphone de contact" )
    mel_contact = CharField(max_length=50, verbose_name = "Courriel de contact" )
    nb_tables = IntegerField(verbose_name = "nombre de tables")
    nb_prises = IntegerField(verbose_name = "nombre de prises de courant")
    nb_personnes = IntegerField(verbose_name = "nombre personnes max.")
    surperficie = IntegerField(verbose_name = "surperficie (m²)")
    couleur = CharField(max_length=10, verbose_name= "Couleur pour le dépliant")
    bus = TextField(verbose_name= "Lignes de bus (format JSON)")
                        

    @property
    def ville(self):
        """
        Extrait le nom de ville depuis l'adresse
        """
        result = ""
        pattern = re.compile(r"\d\d\d\d\d\s+(\S+)")
        for l in re.split(r"\r?\n", self.adresse):
            m = pattern.match(l)
            if m:
                result = m.group(1)
        return result

    @property
    def nom_complet(self):
        """
        Concatène le nom de ville et le nom court
        """
        return f"{self.ville} ({self.nom_court})"
    
    @property
    def fond_clair(self):
        luma = hex_to_yiq(self.couleur.strip())[0]
        return luma > 135
    
    def forjson(self):
        return {
            "nom_court"  : self.nom_court,
            "adresse" : self.adresse,
            "tel_contact" : self.tel_contact,
            "mel_contact" : self.mel_contact,
            "nb_tables" : self.nb_tables,
            "nb_prises": self.nb_prises,
            "nb_personnes": self.nb_personnes,
            "surperficie": self.surperficie,
        }
    
    def __str__(self):
        return f"Repair-Café « {self.nom_court} » : {self.surperficie} m², {self.nb_personnes} personnes max."

class LC(models.Model):
    """
    Pour une relation many to many entre lieux et catégories
    """

    lieu = models.ForeignKey('Repair_Cafe', on_delete=models.CASCADE)
    categorie = models.ForeignKey('Categorie', on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = "Lieux ⇌ Catégories"
        
    def __str__(self):
        return f"{self.lieu.nom_court} ⇌ {self.categorie.nom}"

class Horaire(models.Model):
    """
    Un « horaire habituel »  (classe abstraite)
    """
    class Meta:
       abstract = True
       
    HEURES  = [ (t, f"{t+8:02d}") for t in range(15) ] # de 8 à 23 heures
    MINUTES = [ (t, f"{15*t:02d}") for t in range(4) ] # 0, 15, 30, 45 minutes
    JOURS   = [
        (0, "lundi"),
        (1, "mardi"),
        (2, "mercredi"),
        (3, "jeudi"),
        (4, "vendredi"),
        (5, "samedi"),
        (6, "dimanche"),
    ]
    SEMAINES = [
        (0, "1ère"),
        (1, "2ème"),
        (2, "3ème"),
        (3, "4ème"),
    ]
    
    jour  = models.IntegerField(choices = JOURS, verbose_name="Jour de semaine")
    semaine = models.IntegerField(choices = SEMAINES, verbose_name="Semaine du mois")
    h_deb = models.IntegerField(choices = HEURES, verbose_name="Début (heure)")
    m_deb = models.IntegerField(choices = MINUTES, verbose_name="Début (minute)")
    h_fin = models.IntegerField(choices = HEURES, verbose_name="Fin (heure")
    m_fin = models.IntegerField(choices = MINUTES, verbose_name="Fin (minute)")

    def forjson(self):
        return {
            "jour"  : self.jour,
            "h_deb" : self.h_deb,
            "m_deb" : self.m_deb,
            "h_fin" : self.h_fin,
            "m_fin" : self.m_fin,
        }

    @property
    def nom_jour(self):
        return self.JOURS[self.jour][1]

    @property
    def num_semaine(self):
        return self.SEMAINES[self.semaine][1]
    
    @property
    def habitude(self):
        return mark_safe(f"Le {self.nom_jour}, {self.num_semaine} semaine du mois <br> de {self.HEURES[self.h_deb][1]}:{self.MINUTES[self.m_deb][1]} à {self.HEURES[self.h_fin][1]}:{self.MINUTES[self.m_fin][1]}")
    
    @property
    def debut(self):
        return f"{self.JOURS[self.jour][1]} de {self.HEURES[self.h_deb][1]}:{self.MINUTES[self.m_deb][1]}"
    
    @property
    def fin(self):
        return f"{self.HEURES[self.h_fin][1]}:{self.MINUTES[self.m_fin][1]}"
    
    def __str__(self):
        return f"Horaire habituel : {self.debut} à {self.fin}"

class Repair_Cafe(Lieu, Horaire):
    """
    Un repair-café
    """

    sorte="Repair_Cafe"

    @property
    def tojson(self):
        return json.dumps({
            "lieu"    : Lieu.forjson(self),
            "horaire" : Horaire.forjson(self),
        })

    def habitude_forjson(self):
        return Horaire.forjson(self)

    def __str__(self):
        return f"{Lieu.__str__(self)} ; {Horaire.__str__(self)}"

class Evenement(models.Model):
    """
    un évènement de Repair-café
    """
    sorte = "Evenement"
    
    debut = DateTimeField(verbose_name = "début du RC")
    fin   = DateTimeField(verbose_name = "fin du RC")
    rc  = models.ForeignKey('Repair_Cafe', on_delete=models.CASCADE)

    def __str__(self):
        return f"Repair-Café à {self.rc.nom_court}, le {self.debut.strftime('%Y-%m-%d')} de {self.debut.strftime('%H:%M')} à {self.fin.strftime('%H:%M')}"

    @property
    def ou_quand(self):
        return f"{self.rc.nom_court} {self.debut.strftime('%d/%m/%Y à %H:%M')}"
    
def is_benevole(user):
    """
    Détermine si quelqu'un est bénévole
    @param user une instance de User
    """
    return not user.is_anonymous and \
        (user.is_superuser or bool(Benevole.objects.filter(user =user)))

class Benevole(models.Model):

    sorte = "Benevole"
    
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    profil = TextField(verbose_name = "profil (domaines d'intervention)")

    def __str__(self):
        return f"{self.user} : {self.profil}"

    def save(self, *args, **kw):
        models.Model.save(self, *args, **kw)
        g = Group.objects.get(name='benevole')
        g.user_set.add(self)
        return
    

class Participation_Benevole(models.Model):

    sorte = "Participation_Benevole"
    
    benevole = models.ForeignKey('Benevole', on_delete=models.CASCADE)
    evenement = models.ForeignKey('Evenement', on_delete=models.CASCADE)
    venu = BooleanField(verbose_name = "Est venu")

    class Meta:
        verbose_name_plural = "Participations Bénévoles"

def is_secretaire(user):
    """
    Détermine si quelqu'un est secretaire
    @param user une instance de User
    """
    return not user.is_anonymous and \
        (user.is_superuser or bool(Secretaire.objects.filter(user = user)))

class Secretaire(models.Model):
    sorte = "Secretaire"
    
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    profil = TextField(verbose_name = "profil (géographique ou autre)")

    def __str__(self):
        return f"Secretaire -> {self.user} : {self.profil}"

    def save(self, *args, **kw):
        models.Model.save(self, *args, **kw)
        g = Group.objects.get(name='secretaire')
        g.user_set.add(self)
        return

PRES_CHOICES = (
    ("NC", "Non Connu"),
    ("OUI", "Oui"),
    ("NON", "Non"),
    ("ANNULE", "Annulé"),
)

SX_CHOICES = (
    (" ", "---"),
    ("M", " M "),
    ("F", " F "),
)

CP_CHOICES = (
    ("-----", "Autre"),
    ("59140", "Dunkerque – Centre"),
    ("59140", "Dunkerque – Basse-Ville"),
    ("59140", "Dunkerque"),
    ("59122", "Ghyvelde"),
    ("59122", "Les Moëres"),
    ("59123", "Bray-Dunes"),
    ("59123", "Zuydcoote"),
    ("59153", "Grand-Fort-Philippe"),
    ("59180", "Cappelle-la-Grande"),
    ("59210", "Coudekerque-Branche"),
    ("59229", "Téteghem"),
    ("59229", "Téteghem-Coudekerque-Village"),
    ("59240", "Dunkerque – Rosendaël"),
    ("59240", "Malo-les-Bains"),
    ("59279", "Craywick"),
    ("59279", "Dunkerque – Mardyck"),
    ("59279", "Loon-Plage"),
    ("59380", "Armbouts-Cappel"),
    ("59380", "Coudekerque-Village"),
    ("59380", "Spycker"),
    ("59430", "Dunkerque – Fort-Mardyck"),
    ("59430", "Dunkerque – Saint-Pol-sur-Mer"),
    ("59495", "Leffrinckoucke"),
    ("59630", "Bourbourg"),
    ("59640", "Dunkerque – Petite-Synthe"),
    ("59760", "Grande-Synthe"),
    ("59820", "Gravelines"),
    ("59820", "Saint-Georges-sur-l'Aa"),
)

OK_CHOICES = (
    ("-", "Non connu"),
    ("O", "Oui"),
    ("N", "Non"),
)

class Objet_Reparable(models.Model):

    sorte = "Objet_Reparable"
    
    nom = CharField(max_length=50, verbose_name = "Désignation" )
    poids = FloatField(verbose_name = "Masse (en kg)",
                       blank=True, null=True )
    photo = ImageField(verbose_name = "Photo")
    annonce_le = DateTimeField(verbose_name = "Annoncé le")
    numero_tel = TextField(verbose_name = "N° Tél",
                       blank=True, null=True)
    email = CharField(max_length=50, verbose_name = "Courriel",
                       blank=True, null=True)
    panne = TextField(verbose_name = "Description de la panne",
                       blank=True, null=True)
    demandeur_nom = TextField(verbose_name = "Nom du demandeur")
    demandeur_prenom = CharField(
        max_length=50, verbose_name = "Prénom du demandeur")
    ticket = CharField(
        max_length=6, verbose_name = "N° ticket",
        null=True, blank=True, unique=True)
    horaire_rdv = DateTimeField(
        verbose_name = "Horaire RDV", null=True, blank=True)
    presence = models.CharField(
        verbose_name = "Présence",
        max_length=10, choices=PRES_CHOICES, default="NC")
    sexe = models.CharField(
        verbose_name = "Sexe",
        max_length=1, choices=SX_CHOICES, default=" ")
    localisation = models.CharField(
        verbose_name = "Localisation",
        max_length=6, choices=CP_CHOICES, default="-----")
    tranche_age = models.CharField(
        verbose_name = "Tranche d'âge", max_length=10, null=True, default="")
    reparation = models.CharField(
        verbose_name = "Réparation",
        max_length=1, choices=OK_CHOICES, default="-")
    categorie = models.ForeignKey(
        'Categorie', on_delete=models.CASCADE, blank = True, null = True)
    
    class Meta:
        verbose_name_plural = "Objets Réparables"

    def __str__(self):
        return f"Objet réparable [{self.ticket}] : {self.nom}, annoncé le {self.annonce_le.strftime('%d/%m/%Y')} ; par {self.demandeur_nom} {self.demandeur_prenom}, coordonnées : {self.numero_tel}, catégorie : {self.categorie.name if self.categorie else '?'} if self.categorie else '?'"

    def save(self):
        models.Model.save(self)
        id = str(self.id).encode()
        self.ticket = hashlib.md5(id).hexdigest()[:6]
        models.Model.save(self)

    @property
    def photourl(self):
        return os.path.join(*self.photo.url.split(str(MEDIA_ROOT)+'/'))

class Categorie(models.Model):

    nom = CharField(max_length=50, verbose_name = "Nom de la catégorie")
    exemples = TextField(verbose_name = "Exemples d'objets",
                         blank = True, null = True)
    logo = ImageField(verbose_name = "Logo", upload_to = "logos")
    sans_accent = CharField(max_length=50, verbose_name = "Nom sans accent",
                         blank = True, null = True)

    class Meta:
        verbose_name_plural = "Catégories"

    def save(self):
        self.sans_accent = unidecode.unidecode(self.nom).lower()
        models.Model.save(self)
        return

    def __str__(self):
        return f"Catégorie : {self.nom}"
    
    @property
    def logourl(self):
        return os.path.join(*self.logo.url.split(str(MEDIA_ROOT)+'/')) \
            if self.logo else ""

class Reserve_Reparation(models.Model):

    sorte= "Reserve_Reparation"
    
    reparable = models.OneToOneField('Objet_Reparable', on_delete=models.CASCADE)
    evenement = models.ForeignKey('Evenement', on_delete=models.CASCADE)
    
    class Meta:
        verbose_name_plural = "Réservations pour réparer"

    def __str__(self):
        return f"Réparation : {self.reparable.demandeur_nom} {self.reparable.demandeur_prenom} vient avec un/une {self.reparable.nom} le {self.evenement.debut.strftime('%d/%m/%Y')} ; lieu : « {self.evenement.rc.nom_court} » ({self.evenement.rc.ville})"

    @property
    def confirmations(self):
        return Confirme_Reservation.objects.filter(
            reservation = self).order_by("-confirme_le")

    @property
    def reparations(self):
        return Qui_Repare.objects.filter(
            a_reparer=self).order_by("-propose_le")
    

class Confirme_Reservation(models.Model):

    sorte = "Confirme_Reservation"
    
    secretaire = models.ForeignKey('Secretaire',  on_delete=models.CASCADE)

    confirme_le = DateTimeField(
        verbose_name = "Confirmé le", auto_now_add = True)

    reservation = models.ForeignKey('Reserve_Reparation',  on_delete=models.CASCADE)
    commentaire = TextField(verbose_name = "Commentaires : contacts pris")

    def __str__(self):
        return f"Confirmé le {self.confirme_le} ; par {self.secretaire}, ticket = {self.reservation.reparable.ticket}"

    @property
    def bref(self):
        """
        Commentaire abrégé
        """
        maxlen = 25
        if len(self.commentaire) <= maxlen - 3:
            return self.commentaire
        else:
            return self.commentaire[:maxlen - 3] + "..."
    
class Qui_Repare(models.Model):

    sorte = "Qui_Repare"
    
    a_reparer = models.ForeignKey('Reserve_Reparation',  on_delete=models.CASCADE)
    benevole = models.ForeignKey('Benevole',  on_delete=models.CASCADE)
    propose_le = DateTimeField(
        verbose_name = "Proposé le", auto_now_add = True)
    commentaire = TextField(verbose_name = "Commentaires techniques")

    @property
    def bref(self):
        """
        Commentaire abrégé
        """
        maxlen = 25
        if len(self.commentaire) <= maxlen - 3:
            return self.commentaire
        else:
            return self.commentaire[:maxlen - 3] + "..."

    
    class Meta:
        verbose_name_plural = "Bénévoles inscrits pour une réparation"

class Reparation_Faite(models.Model):

    sorte = "Reparation_Faite"
    
    a_reparer = models.ForeignKey('Reserve_Reparation',  on_delete=models.CASCADE)
    benevole = models.ForeignKey('Benevole',  on_delete=models.CASCADE)
    commentaire = TextField(verbose_name="Ce qui a été réparé")
    photo = ImageField(verbose_name = "Photo facultative", null=True, blank=True)
    
    class Meta:
        verbose_name_plural = "Réparations faites"

class RC_Log(models.Model):

    date = DateTimeField(auto_now_add = True)
    sorte = CharField(max_length=25)
    details = TextField()
    action = CharField(max_length=25, blank = True, default = "")

    def __str__(self):
        return f"""\
{self.date} : {self.sorte}{"_" + self.action if self.action else ""}
    {self.details}
"""

    class Meta:
        verbose_name_plural = "Journaux"

def secretaire_ou_plus(user):
    """
    renvoie une instance de Secretaire ; si l'utilisateur est administrateur,
    ça l'inscrit à la volée chez les secrétaires ; sinon ça renvoie None
    @param user un nom d'utilisateur
    @return une instance de Secretaire
    """
    secretaires = Secretaire.objects.filter(user = user)
    if secretaires:
        return secretaires[0]
    else :
        utilisateurs = User.objects.filter(username = user)
        if utilisateurs and utilisateurs[0].is_superuser:
            new = Secretaire(user = user, profil = "administrateur.e")
            new.save()
            return new
    return None
