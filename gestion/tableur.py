# fichier tableur.py
#
# Copyright (C) 2025 Georges Khaznadar <georgesk@debian.org>
#
# Ce programme a pour but de construire un fichier ODS basé sur
# le fichier /media/docs/rapport-modele.ods, contenant les données d'un rapport
# d'évènement de repair-café
#

from repaircafe.settings import MEDIA_ROOT, PURE_BASE_DIR
import tempfile, subprocess, re, copy, time, argparse, sys, os, gettext
from xml.dom import minidom
from collections import OrderedDict

_ = gettext.gettext

# structure des colonnes dans le tableau, et liste de leurs titres
COLONNES = OrderedDict(enumerate([
    "Nom","Prénom","N°tel","Mail ",
    "@@CATEGORIE@@",
    "Type de panne","Horaire RDV","RDV pris le","Présence",
    "Sexe","Localisation","Newsletter","Tranche d'âge","Réparation","Kg"
]))

# la colonne des catégories a un titre qui varie
CATEGORIE_COL = 4 # n° de colonne des titres de catégorie

# les catégories de réparations sont :
CATEGORIES = [
"Électroménager",
"Ordinateur",
"Mécanique",
"Hifi",
"Couture",
"Horlogerie",
"Machines à coudre",
]

def get_children(node, func):
    if not node:
        yield None
    for child in node.childNodes:
        if func(child):
            yield child
        yield from get_children(child, func)

def get_text(node):
    """
    Récupère le texte sous un nœud donné
    @param node le nœud
    @return tout le texte trouvé sous ce nœud (récursivement); s'il y
      a plusieurs morceaux de texte, ill sont rassemblés avec des espaces
      intercalaires. S'il n'y a aucun texte, ça renvoie None
    """
    texts_nodes = get_children(node, lambda n: n.nodeType == n.TEXT_NODE)
    text_value = ' '.join([str.strip(t.nodeValue) for t in texts_nodes])
    return text_value if text_value else None

def lignes_modeles(doc, repere = "@"):
    """
    Renvoie une liste d'instance représentant des éléments <table:table-row>
    dont la dernière contient, dans une cellule au moins, un caractère
    de repérage. Si le nœud contient plusieurs instance de <table:table>
    seule la première est prise en compte.
    
    @param doc un nœud, instance renvoyée par minidom.parse()
    @param repere un caractère pour repérer le dernière ligne modèle.
           Ce caractère est "@" par défaut
    @return une liste de copies d'instances de Node, de type <table:table-row>
    """
    tables = doc.getElementsByTagName("table:table")
    if not tables:
        return []
    rows= tables[0].getElementsByTagName("table:table-row")
    last = len(rows)
    for i, row in enumerate(rows[::-1]):
        t = get_text(row) or ""
        if repere in t:
            last = last-i
            break
    return copy.deepcopy(rows[:last])

def cellules_modeles(row, repere = "@"):
    """
    Renvoie une liste d'instance représentant des éléments
    <table:table-cell> dont la dernière contient, un caractère de
    repérage.
    
    @param row un nœud, instance représentant un élément <table:table-row>
    @param repere un caractère pour repérer le dernière cellule modèle.
           Ce caractère est "@" par défaut
    @return une liste de copies d'instances de Node, de type <table:table-cell>
    """
    cells= row.getElementsByTagName("table:table-cell")
    last = len(cells)
    for i, cell in enumerate(cells[::-1]):
        t = get_text(cell) or ""
        if repere in t:
            last = last-i
            break
    return copy.deepcopy(cells[:last])

def vide_tableau(doc, repere = "@"):
    """
    Vide un tableau de toutes ses lignes (par effet de bord), et renvoie
    la liste des lignes utilisables comme modèle, d'après la présence d'un
    repère, à la dernière de ces lignes-modèle.
    @param un nœud, instance renvoyée par minidom.parse()
    @param repere un caractère pour repérer le dernière ligne modèle.
           Ce caractère est "@" par défaut
    @return la liste des lignes utilisables comme modèle
    """
    lm = lignes_modeles(doc, repere)
    tables = doc.getElementsByTagName("table:table")
    rows= tables[0].getElementsByTagName("table:table-row")
    for row in rows[::-1]:
        p = row.parentNode
        p.removeChild(row)
        row.unlink()
    return lm

def vide_ligne (row, repere = "@"):
    """
    Vide une ligne de ses cellules(par effet de bord), et renvoie
    la liste des cellules utilisables comme modèle, d'après la présence d'un
    repère, à la dernière de ces cellules-modèle.
    @param row un nœud, instance représentant un element <table:table-row>
    @param repere un caractère pour repérer le dernière cellule modèle.
           Ce caractère est "@" par défaut
    @return la liste des cellules utilisables comme modèle
    """
    lc = lignes_modeles(doc, repere)
    cells= row.getElementsByTagName("table:table-cell")
    for cell in cells[::-1]:
        p = cell.parentNode
        p.removeChild(cell)
    return lc

def replace_in_cells(doc, model, replacement):
    """
    Remplace dans toutes les cellules d'un tableau
    @param doc un document qui sera modifié par effet de bord
    @param model un expression régulière
    @param replacement remplacement quand l'expression régulière est trouvée
    """
    pattern = re.compile(model)
    cells = doc.getElementsByTagName("table:table-cell")
    for cell in cells:
        texts = get_children(cell, lambda n: n.nodeType == n.TEXT_NODE)
        for t in texts:
            val = re.sub(model, replacement, t.nodeValue)
            if val != t.nodeValue:
                t.nodeValue = val
    return

def append_row(table, model_row, empty = False, replace = None, repere = "@"):
    """
    Ajoute une ligne à la fin d'un tableau.
    @param table une instance qui représente un element <table:table>
    @param model_row une instance qui représente un element <table:table-row>
      on ne travaillemera que sur une copie(profonde) de cette ligne
    @param empty s'il faut vider la ligne de ses cellules ; par défaut : False
    @param replace s'il faut remplacer une ou des cellules de la ligne, selon
      une séquence de paires (regexp, remplacement) ; par défaut: None
    @param repere un caractère pour repérer le dernière cellule modèle.
           Ce caractère est "@" par défaut
    """
    row = copy.deepcopy(model_row)
    if empty:
        vide_ligne(row, repere)
    if replace is not None:
        for model, replacement in replace:
            replace_in_cells(row, model, replacement)
    table.appendChild(row)
    return

VERBOSE = False
START = time.time()

def log(*args, **kw):
    if VERBOSE:
        print("%4.1f" %(time.time() -START), "s :", *args, **kw)
    return

def makeSheet(infile = "modele0.ods", outfile = "essai.ods", repere = "@",
              verbose = False, **kw):
    """
    Crée un tableur, en prenant comme point de départ un fichier modèle
    @param infile : le fichier modèle, "modele0.ods" par défaut
    @par outfile : le fichier de sortie, "essai.ods" par défaut
    @param repere : un caractère pour distinguer les champs à remplacer,
      "@" par défaut
    @param verbose : booléen pour augmenter la verbosité (False par défaut)
    @param **kw un dictionnaire (mots-clés => valeurs)
    """
    global VERBOSE, START
    VERBOSE = verbose
    START=time.time()
    with tempfile.NamedTemporaryFile(prefix="repair", suffix=".xml") as tmpfile:
        cmd = ['odf2xml', "-o", tmpfile.name, infile]
        cp = subprocess.run(cmd)
        doc = minidom.parse(tmpfile.name)
        log(_("%(infile)s parsed") %dict(infile=infile))
        with tempfile.NamedTemporaryFile(prefix="repair", suffix=".xml") as \
             tmpoutfile:
            lm = vide_tableau(doc, repere)
            log(_("%(length)s template lines extracted")
                % dict(length = len(lm)))
            """
            on sait que lm[0] contient @@RC@@ et @@Date@@, lm[1] est
            une ligne vide, et que lm[2] contient une liste de titres
            sur fond coloré, dont @@Categorie@@ en colonne 4
            """
            l0 = lm[0] # titres
            l1 = lm[1] # ligne vide
            l2 = lm[2] #sous-titres
            table = doc.getElementsByTagName("table:table")[0]
            append_row(table, l0, replace =(
                ("@@RC@@", kw.get("rc", "Repair-Café inconnu ?")),
                ("@@Date@@", kw.get("date", "date inconnue ?")),
            ))
            log(_("title line is set"))
            categories = kw.get("categories", CATEGORIES)
            for cat in categories:
                append_row(table, l1)
                append_row(table, l2, replace = (
                    ("@@Categorie@@", cat),
                ))
                log(_('category %(cat)s is set') % dict(cat=cat))
            tmpoutfile.write(doc.toxml().encode("utf-8"))
            basename = re.sub(r'\.ods$', "", outfile)
            outfile = basename + ".ods"
            cmd = ['xml2odf', "-o", basename, "-s", tmpoutfile.name]
            cp = subprocess.run(cmd)
            log(_("created"), outfile)
    return

def localize(package = None):
    """
    localisation
    @param package nome du paquet; si celui-ci n'est pas défini
      on se base sur le nom de l'exécutable
    """
    if package is None:
        package = os.path.basename(__file__)
        package = re.sub(r"\.py$", "", package)
    localedir = str(PURE_BASE_DIR) + "/lang/usr/share/locale"
    if os.path.exists(localedir):
        gettext.bindtextdomain(package, localedir = localedir)
    else:
        gettext.bindtextdomain(package)
    gettext.textdomain(package)
    return

def parse_args():
    """
    crée un analyseur de paramètres de la ligne de commande et le lance
    @return une instance de Namespace
    """
    parser = argparse.ArgumentParser(
        prog='Essai.py',
        description='crée un tableur ODS pour les données d\'un Repair-café',
        epilog='Copyright © 2025 Georges Khaznadar <georgesk@debian.org>')
    parser.add_argument(
        '-v', '--verbose',
        help = 'Émet quelques messages (verbosité)',
        action='store_true')
    parser.add_argument(
        "infile",
        help = 'Fichier modèle de départ',)
    parser.add_argument(
        '-o', '--output', required = True,
        help ='Nom du fichier à produire')
    parser.add_argument('--version', action='version', version='%(prog)s 1.0')
    params = parser.parse_args(sys.argv[1:])
    return params

def get_packagename():
    """
    prend le nom de paquet dans lang/Makefile
    """
    cmd = "grep ^PACKAGE " + str(PURE_BASE_DIR) + "/lang/Makefile | awk -F ' = ' '{print $2}'"
    cp = subprocess.run(
        cmd, shell=True, capture_output = True, encoding = "utf-8")
    return cp.stdout.strip()

if __name__ == "__main__":
    from gettext import gettext as _
    localize(get_packagename())
    params = parse_args()
    makeSheet(
        verbose = params.verbose,
        infile = params.infile, outfile = params.output)
