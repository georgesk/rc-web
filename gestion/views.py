from django.shortcuts import render, redirect
from django.http import JsonResponse, FileResponse, HttpResponse
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import ensure_csrf_cookie
from django.template.loader import render_to_string
import django.contrib.auth

from .forms import *
from .models import *
from .calendrier import *
from .tableur import *
from repaircafe.settings import MEDIA_ROOT, TZINFO

import hashlib
import os
from base64 import b64decode
import collections
import io
import zipfile

def log(obj, sorte="", action=""):
    """
    Enregistre une ligne de journal pour un objet obj
    @param obj un modèle
    @param sorte un qualificatif pour l'objet, pour cette ligne de journal
    @param action un qualificatif pour l'action : "delete" ou "write" ...
    """
    if not sorte and hasattr(obj, 'sorte') and type(obj.sorte) == str:
        sorte = obj.sorte
    details = ""
    try:
        details = obj.tojson
    except:
        details = str(obj)
    rclog = RC_Log(sorte=sorte, details = details, action = action)
    rclog.save()
    return

def premiers_joursem(debut, jour, semaine, peut_deroger = False, dejapris = []):
    """
    Générateur de dates qui sont le premier jour calendaire de la
    nième semaine de mois consécutifs, en évitant des dates déjà prises.
    @param debut une date de début
    @param jour entier ;    un jour calendaire   comme dans Horaire.JOURS
    @param semaine entier ; un numéro de semaine comme dans Horaire.SEMAINES
    @param peut_deroger (faux par défaut) si c'est vrai, alors la date de
      debut est renvoyée en premier même si celle-ci n'est pas une
      « date compatible »
    @param dejapris un itérable de dates déjà prises, à éviter
    @yield une instance de date
    """
    def date_compatible():
        """
        calcule la premier jour du mois et le nombre de jours d'écart
        entre celui-ci et le couple jour/semaine désiré.
        @return une instance de date qui a le bon jour et la bonne semaine
        """
        premier_du_mois = date(year=an, month=mois, day=1)
        jours_ecart = (7 + jour - premier_du_mois.weekday()) %7 + semaine * 7
        return premier_du_mois + timedelta(days = jours_ecart)

    def mois_suivant(mois, an):
        mois += 1
        if mois > 12:
            mois = 1
            an +=1
        return mois, an
        
    an = debut.year
    mois = debut.month
    if peut_deroger:
        dc = debut
    else:
        dc = date_compatible()
    while True:
        while dc < debut or dc in dejapris:
            mois, an = mois_suivant(mois, an)
            dc = date_compatible()
        yield dc
        mois, an = mois_suivant(mois, an)
        dc = date_compatible()

def secretaire(request):
    return request.user.is_superuser or \
        request.user.groups.filter(name='secretaire').exists()

def benevole(request):
    return request.user.is_superuser or \
        request.user.groups.filter(name='benevole').exists()

TROIS_MOIS = 92 # quatre vingt douze jours est plus long ou égal à trois mois

# Create your views here.

def index(request):
    return render(
        request,
        'gestion/index.html',
        {
            "nom_page": 'Accueil',
            "version":  'RC-web 0.1',
            "page_pour_tous": True,
            "is_secretaire": secretaire(request),
            "is_benevole": benevole(request),
        }
    )

def calendrier(request):
    """
    Un calendrier PDF, sur quelques mois. GET apporte les paramètres
    debut et duree ; si debut est nul, le premier mois est le mois courant,
    si debut vaut -1, le premier mois est six mois dans le futur
    si debut est positif, c'est le plus proche mois de ce numéro par rapport
    à la date courante ; duree est le nombre de mois à représenter. Ces deux
    paramètres ont les valeurs 0 et 6 par défaut, respectivement.
    """
    
    filename = "calendrier-repair-cafe.pdf"
    aujourdhui = date.today()
    deb = int(request.GET.get("debut", "0"))
    dur = int(request.GET.get("duree", "6"))

    # par défaut, <dur> mois à partir du mois courant
    annee = aujourdhui.year
    mois = aujourdhui.month

    # si deb == -1, saut de 6 mois dans le futur
    if deb == -1 :
        mois = aujourdhui.month + 6
        if mois > 12:
            mois -= 12
        deb = mois
    # si deb !=0, <dur> mois à partir du mois <deb> le plus proche
    if deb > 0:
        if deb - aujourdhui.month > 6 :
            # pas de futur à plus de 6 mois en avant
            annee = aujourdhui.year -1
        else:
            annee = aujourdhui.year
    # annee et mois sont connus on fait une liste de (annee, mois) consécutifd
    liste_mois = []
    for i in range(dur):
        liste_mois.append((annee, mois))
        mois += 1
        if mois > 12:
            annee += 1
            mois -= 12
    
    tm = [] # liste de tableaux de mois avec leurs couleurs
    for annee, mois in liste_mois:
        jours_couleurs = {} # dictionnaire jour => couleur
        debut = datetime(annee, mois, 1, tzinfo = TZINFO)
        if mois < 12:
            fin = datetime(annee, mois+1, 1, tzinfo = TZINFO)
        else: # le début est en décembre : donc avant janvier l'an prochain
            fin = datetime(annee+1, 1, 1, tzinfo = TZINFO)
        evenements = Evenement.objects.filter(
            debut__gte = debut, debut__lt = fin)
            
        for ev in evenements:
            if ev.debut.day in jours_couleurs:
                jours_couleurs[ev.debut.day].append(ev.rc.couleur)
            else:
                jours_couleurs[ev.debut.day] = [ev.rc.couleur]
        couleurs = [Couleur_Du_Jour(k,*v) for k,v in jours_couleurs.items()]
        tm.append(Table_Mois(annee, mois, couleurs = couleurs))
        
    ci = calendrierImprimable(*tm)
    return FileResponse(ci, as_attachment=True, filename=filename)

def donnees_utilisateur(request):
    """
    Modification des données utilisateur
    """
    if request.POST:
        form= donnees_utilisateur_Form(request.POST)
        if form.is_valid():
            request.user.last_name = form.cleaned_data.get("nom")
            request.user.first_name = form.cleaned_data.get("prenom")
            request.user.email = form.cleaned_data.get("email")
            if form.cleaned_data["passe1"]:
                request.user.set_password(form.cleaned_data["passe1"])
            request.user.save()
            log(request.user, sorte = "Donnees_Utilisateur", action = "write")
            return redirect(form.cleaned_data["next"])
    else:
        next = request.GET.get("next")
        form= donnees_utilisateur_Form({
            "nom": request.user.last_name,
            "prenom": request.user.first_name,
            "email": request.user.email,
            "next": next,
        })
    return render(
        request,'gestion/donnees_utilisateur.html',
        {
            "form": form,
            "nom_page": "Mise à jour des données personnelles pour " + request.user.username,
        })

def lieux(request):
    """
    Donne la liste des lieux de repair-cafés et les dates d'ouverture

    Quand on est administrateur, on peut créer/supprimer des lieux et
    éditer des dates d'ouverture
    """
    message = ""    # colorisation du message selon level ...
    level = "plain" # autres possibilités : "warning", "error"
    a_rouvrir = ""

    ###########################################
    # Suppression d'un évènement
    ###########################################
    if "del_event" in request.POST:
        id=int(request.POST.get("event_id"))
        evt = Evenement.objects.get(pk = id)
        log(evt, action = "delete")
        evt.delete()

    ###########################################
    # Création/édition d'un lieu de Repair-café
    ###########################################
    if "submit_lieu" in request.POST:
        id=int(request.POST.get("temp_id"))
        if id > 0:
            instance = Repair_Cafe.objects.get(pk=int(id))
            form = RepairCafeForm(request.POST, instance=instance)
        else:
            form = RepairCafeForm(request.POST)
        if form.is_valid():
            form.save()
            log(form.instance, action = "write")
            nom = request.POST.get('nom_court')
            message = f"Enregistré les données du Repair-Café « {nom} »"

    ###########################################
    # Suppression d'un lieu de Repair-café
    ###########################################
    if "del_lieu" in request.POST:
        id=int(request.POST.get("temp_id"))
        instance = Repair_Cafe.objects.get(pk=int(id))
        nom = request.POST.get("lieu")
        if nom == instance.nom_court:
            log(instance, action = "delete")
            instance.delete()
            message = f"Supprimé du Repair-Café « {nom} »,<br>trop tard pour les récupérer"
            level = "warning"


    ###########################################
    # mise en place de nouvelles dates
    ###########################################
    if "submit_horaires" in request.POST:
        id = int(request.POST.get("temp_id"))
        lieu = Repair_Cafe.objects.get(pk = id)
        repetition = int(request.POST.get("repetition"))
        h_d = int(request.POST.get("h_d"))
        m_d = int(request.POST.get("m_d"))
        h_f = int(request.POST.get("h_f"))
        m_f = int(request.POST.get("m_f"))
        jours_compatibles = premiers_joursem(
            date.fromisoformat(request.POST.get("date")),
            lieu.jour,
            lieu.semaine,
            peut_deroger = True,
            dejapris = [ev.debut.date()
                        for ev in Evenement.objects.filter(rc = lieu)],
        )
        les_dates = (next(jours_compatibles) for _ in range(repetition))
        for d in les_dates:
            dt = datetime.combine(
                d, datetime.min.time(),
                tzinfo = TZINFO)
            debut = dt + timedelta(hours= 8 + h_d, minutes = 15 * m_d)
            fin   = dt + timedelta(hours= 8 + h_f, minutes = 15 * m_f)
            ev = Evenement(rc = lieu, debut = debut, fin = fin)
            ev.save()
            log(ev, action = "write")
            
    lieux = sorted(list(Repair_Cafe.objects.all()),
                   key = lambda rc: rc.ville)
    for l in lieux:
        l.categories = [lc.categorie for lc in LC.objects.filter(lieu = l)]
    
    def events(l, is_secretaire):
        """
        Instances d'Evenement à montrer pour le lieu l, selon qu'on
        est secretaire (on montre tout) ou qu'on ne l'est pas (limitation
        à deux mois dans le futur)
        """
        result = Evenement.objects.filter(rc=l)
        if not is_secretaire :
            maintenant = timezone.localtime(timezone.now())
            dans_trois_mois = maintenant + timedelta(days=TROIS_MOIS)
            result = result.filter(debut__gt = maintenant,
                                   debut__lt = dans_trois_mois)
        return result
    
    data_lieux = {
        l.nom_court: {
            "evenements" : sorted([e for e in events(l, secretaire(request))],
                                  key = lambda ev: ev.debut),
            "lieu" : l,
        }
        for l in lieux
    }
    categories = Categorie.objects.order_by("nom")
    return render(
        request,'gestion/lieux.html',
        {
            "nom_page": "Lieux des Repair-Cafés",
            "data_lieux": data_lieux,
            "secretaire": secretaire(request),
            "message": mark_safe(message),
            "level": level,
            "a_rouvrir" : a_rouvrir,
            "page_pour_tous": True,
            "version":  'RC-web 0.1',
            "categories": categories,
        })

def lieuform(request):
    """
    Renvoie un formulaire pour modifier ou créer un repair-café
    selon que le POST contient ou non un paramètre rc
    (pour le nom du repair-café)
    """
    id = 0
    nom = request.POST.get("rc", "")
    if nom:
        instance = Repair_Cafe.objects.get(nom_court = nom)
        id = instance.id
        form = RepairCafeForm(None, instance=instance)
    else:
        form = RepairCafeForm()
    data = {
        "html": render_to_string(
            'gestion/lieuform.html',
            {"form": form,}),
        "id": id,
    }
    return JsonResponse(data)

def del_lieuform(request):
    """
    Renvoie un formulaire pour modifier ou créer un repair-café
    selon que le POST contient ou non un paramètre rc
    (pour le nom du repair-café)
    """
    id = 0
    nom = request.POST.get("rc", "")
    id = Repair_Cafe.objects.get(nom_court = nom).id
    form = Del_Lieu_Form()
    data = {
        "html": render_to_string(
            'gestion/del_lieuform.html',
            {"form": form, "nom": nom,}),
        "id": id,
    }
    return JsonResponse(data)

def horaires_lieuform(request):
    """
    Renvoie un formulaire pour créer des dates pour un repair-café
    le POST contient un paramètre rc
    (pour le nom du repair-café)
    """
    id = 0
    nom = request.POST.get("rc", "")
    lieu = Repair_Cafe.objects.get(nom_court = nom)
    id = lieu.id
    form = Horaire_LieuForm()
    # on détermine la première date compatible avec les
    # préférence du Repair-café
    ladate = next(premiers_joursem(
        date.today(), lieu.jour, lieu.semaine,
        dejapris = list(
            ev.debut.date() for ev in Evenement.objects.filter(rc=lieu)),
    ))
    
    data = {
        "html": render_to_string(
            'gestion/horaires_lieuform.html',
            {
                "form": form,
                "nom": nom,
                "lieu": lieu,
            }),
        "id": id,
        "date": ladate,
        "habitude": lieu.habitude_forjson(),
    }
    return JsonResponse(data)

def affiche_evenement(request):
    """
    Renvoie un bloc html relatif à un évènement. Le POST contient
    l'identifiant d'un évènement.
    """
    id = int(request.POST.get("id"))
    sec = request.POST.get("secretaire") == 'true'
    evt = Evenement.objects.get(pk = id)
    
    data = {
        "html": render_to_string(
            'gestion/affiche_evenement.html',
            {
                "evt": evt,
                "lieu": evt.rc.nom_court,
                "secretaire": sec,
            }),
        "id": id,
    }
    return  JsonResponse(data)

def signup(request):
    """
    page de connexion
    """
    if request.POST:
        next = request.POST.get("next")
        username = request.POST.get("username")
        passw = request.POST.get("passw")
        user = django.contrib.auth.authenticate(
            username=username, password=passw)
        if user is not None:
            django.contrib.auth.login(request, user)
            return redirect(next)
        else:
            form = LoginForm(request.POST)
    else:
        next = request.GET.get("next")
        form = LoginForm()
    return render(
        request,'gestion/login.html',
        {
            "next": next,
            "form": form,
        }
    )

def signoff(request):
    django.contrib.auth.logout(request)
    next = request.GET.get("next")
    if next:
        return redirect(next)
    else:
        return redirect("/")
    
def rdv(request):
    """
    Page de prise de rendez-vous
    """
    confirmes = []
    ########## on confirme un choix d'évènement de repair-café pour un ticket
    if "confirme_choix" in request.POST:
        ev = Evenement.objects.get(id = int(request.POST.get("event_id")))
        orep = Objet_Reparable.objects.get(ticket = request.POST.get("ticket"))
        if not Reserve_Reparation.objects.filter(reparable = orep):
            rr = Reserve_Reparation(reparable = orep, evenement = ev)
            rr.save()
            log(rr, action = "write")
            confirmes = rr.confirmations
        return render(
            request, "gestion/ticket.html", {
                "nom_page" : "Ticket de rendez-vous",
                "page_pour_tous": True,
                "version":  'RC-web 0.1',
                "orep": orep,
                "evenements" : None,
                "ev" : ev,
                "ticket": True,
                "confirmes": confirmes,
            }
        )

    ########## première étape pour créer un ticket
    if "submit_repare" in request.POST:
        form = DemandeReparationForm(request.POST)
        if form.is_valid():
            path=""
            # récupère la photo s'il y en a ine, sinon prend "rien.svg"
            if "base64," in request.POST.get("src"):
                _, encoded = request.POST.get("src").split("base64,", 1)
                data = b64decode(encoded)
                filename = f"demande_{datetime.now().isoformat()}.png"
                path = os.path.join("photos", filename)
                with open(os.path.join(MEDIA_ROOT, path), "wb") as outfile:
                    outfile.write(data)
            else:
                path = os.path.join("photos", "rien.svg")
            orep = Objet_Reparable(
                nom=form.cleaned_data.get("nom"),
                poids=form.cleaned_data.get("poids"),
                annonce_le=datetime.today(),
                panne=form.cleaned_data.get("panne"),
                demandeur_nom=form.cleaned_data.get("demandeur_nom"),
                demandeur_prenom=form.cleaned_data.get("demandeur_prenom"),
                numero_tel = form.cleaned_data.get("tel"),
                email = form.cleaned_data.get("mel"),
                photo=path)
            orep.save()
            log(orep, action = "write")
            ticket = orep.ticket
        else:
            # le formulaire n'est pas valide ?
            print("GRRRR formulaire invalide")
        # passe au rendu du ticket incomplet, pour lequel il manque
        # encore le choix d'un évènement
        maintenant = timezone.localtime(timezone.now())
        dans_trois_mois = maintenant + timedelta(days=TROIS_MOIS)
        lieux = sorted(list(Repair_Cafe.objects.all()),
                   key = lambda rc: rc.ville)
        evenements = collections.OrderedDict(
            (
                (rc.nom_complet, {
                    "rc": rc,
                    "ev": Evenement.objects.filter(
                        rc = rc,
                        debut__gt = maintenant,
                        debut__lt = dans_trois_mois)
                 }) for rc in sorted(list(Repair_Cafe.objects.all()),
                                     key = lambda rc: rc.nom_complet)
            )
        )
        return render(
            request, "gestion/ticket.html", {
                "nom_page" : "Ticket de rendez-vous",
                "page_pour_tous": True,
                "version":  'RC-web 0.1',
                "orep": orep,
                "evenements" : evenements,
                "ev": None,
                "ticket": False,
            }
        )

    # pas de formulaire posté
    form = DemandeReparationForm()

    return render(
        request, "gestion/rdv.html", {
            "nom_page" : "Rendez-vous",
            "page_pour_tous": True,
            "version":  'RC-web 0.1',
            "form": form,
        }
    )

def ticket(request):
    orep = None
    ev=None
    confirmes=[]
    evenements = None
    if request.POST:
        form = TicketForm(request.POST)
        if form.is_valid():
            ticket = form.cleaned_data.get("ticket")
            oreps = Objet_Reparable.objects.filter(ticket = ticket)
            if oreps:
                orep = oreps[0]
                rrs = Reserve_Reparation.objects.filter(reparable = orep)
                if rrs:
                    rr = rrs[0]
                    confirmes = rr.confirmations
                    ev = rr.evenement
    else:
        form = TicketForm()
    return render(
        request, "gestion/ticket.html", {
            "nom_page" : "Ticket de rendez-vous",
            "page_pour_tous": True,
            "version":  'RC-web 0.1',
            "orep": orep,
            "confirmes": confirmes,
            "ticket": True,
            "form": form,
            "orep": orep,
            "evenements" : None,
            "ev" : ev,
        }
    )

def choisit_evenement(request):
    """
    prépare un dialogue pour choisir (ou pas) un évènement pour un objet à
    réparer. Le POST fournit les paramètres ev et ticket
    """
    deja_choisi = False
    evenement = Evenement.objects.get(id=int(request.POST.get("ev")))
    orep = Objet_Reparable.objects.get(ticket=request.POST.get("ticket"))
    if Reserve_Reparation.objects.filter(reparable = orep):
        deja_choisi = True
    data = {
        "html": render_to_string(
            'gestion/choisit_evenement.html',
            {
                "evt": evenement,
                "orep": orep,
                "deja_choisi": deja_choisi,
            }),
        "ev": evenement.id,
        "ticket": orep.ticket,
    }
    return  JsonResponse(data)
    
def tous_tickets(request):
    """
    Affiche tous les tickets pour les secrétaires, tous les tickets confirmés
    pour les bénévoles ; pas visible pour les autres
    Le POST peut contenir des paramètres "gestion", "comment"
    """
    oreps = Objet_Reparable.objects.all()
    # déjà, on nettoie tous les objets à réparer où la déclaration
    # est incomplète depuis une heure au moins.
    lieu = request.session.get("lieu","")
    date1 = request.session.get("date_travail1","")
    date2 = request.session.get("date_travail2","")
    start = timezone.localtime(timezone.now())
    if date1:
        d1 = timezone.make_aware(datetime.strptime(date1,"%d-%m-%Y"),
                                 timezone=TZINFO)
        start = min(start, d1)
    for o in oreps:
        if not Reserve_Reparation.objects.filter(reparable = o) \
           and o.annonce_le < start - timedelta(hours=1):
            try:
                path = str(o.photo.file)
                if os.path.exists(path) and "rien.svg" not in path:
                    # on efface le fichier existant, sauf
                    # si c'est le fichier par défaut, quand il n'y a pas
                    # de photo
                    os.unlink(path)
            except:
                pass
            o.delete()
    # nettoyage terminé, on regarde le POST
    if "changement" in request.POST and \
       request.POST.get("changement") == "change":
        # ça vient d'un.e secrétaire qui veut changer un ticket déjà confirmé
        ticket = request.POST.get("ticket")
        return redirect("secretaire_modif_ticket", ticket)
    if "gestion" in request.POST: # ça vient d'un.e secrétaire
        gestion = request.POST.get("gestion")
        comment = request.POST.get("comment")
        ticket = request.POST.get("ticket")
        secretaire = secretaire_ou_plus(request.user)
        if gestion == "nochange":
            # le/la secrétaire confirme sans changement
            rr = Reserve_Reparation.objects.get(reparable__ticket = ticket)
            cf = Confirme_Reservation(
                secretaire = secretaire, reservation = rr,
                commentaire = comment)
            cf.save()
        else: # gestion == "change" if faut rediriger vers une autre page
            return redirect("secretaire_modif_ticket", ticket)
    # on prend en compte toutes les réservations après la date `start`
    rrs = Reserve_Reparation.objects.filter(
        evenement__debut__gt = start).order_by(
        'evenement__debut', 'evenement__rc__nom_court')
    rrs_filtres = rrs
    filtrage="" ## phrase pour expliquer le filtrage
    ndates = 2  ## nombre de dates à prendre en compte
    if date2 == date1 or not date2:
        ndates = 1
    if not date1:
        ndates = 0
    if lieu and lieu != "all":
        rrs_filtres = rrs_filtres.filter(evenement__rc__nom_court = lieu)
        filtrage += f"« {lieu} »,"
    else:
        filtrage += "la date seulement,"
    if ndates == 2:
        filtrage += f" entre le {date1} et le {date2}"
    elif ndates == 1:
        filtrage += f" le {date1}"
    else:
        filtrage = "rien du tout"
    if date1:
        rrs_filtres = rrs_filtres.filter(evenement__debut__gte = datetime.strptime(date1,"%d-%m-%Y"))
    if date2 and date2 != date1:
        rrs_filtres = rrs_filtres.filter(evenement__debut__lte = datetime.strptime(date2,"%d-%m-%Y"))
    return render(
        request, "gestion/tous_tickets.html", {
            "nom_page" : "Tous les tickets de rendez-vous",
            "version":  'RC-web 0.1',
            "compte_rrs" : rrs.count(),
            "rrs_filtres" : rrs_filtres,
            "compte_rrs_filtres" : rrs_filtres.count(),
            "filtrage": filtrage,
            'is_secretaire': is_secretaire(request.user),
            'is_benevole': is_benevole(request.user),
        }
    )

def secretaire_pour_ticket(request):
    """
    Mise en place du dialogue pour qu'un.e secrétaire gère un ticket
    Le POST contient un parametre ticket
    """
    ok = True
    try:
        ticket = request.POST.get("ticket")
        rr = Reserve_Reparation.objects.get(reparable__ticket = ticket)
        confirmes = Confirme_Reservation.objects.filter(
            reservation = rr).order_by("-confirme_le")
    except:
        ok=False
    data = {
        "html": render_to_string(
            'gestion/secretaire_pour_ticket.html',
            {
                "rr": rr,
                "confirmes": confirmes,
            }),
        "ok": ok,
    }
    return  JsonResponse(data)

def benevole_pour_ticket(request):
    """
    Mise en place du dialogue pour qu'un.e bénévole gère un ticket
    Le POST contient un parametre ticket
    """
    ok = True
    try:
        ticket = request.POST.get("ticket")
        rr = Reserve_Reparation.objects.get(reparable__ticket = ticket)
    except:
        ok=False
    data = {
        "html": render_to_string(
            'gestion/benevole_pour_ticket.html',
            {
                "rr": rr,
            }),
        "ok": ok,
    }
    return  JsonResponse(data)

def secretaire_pour_ticket(request):
    """
    Mise en place du dialogue pour qu'un.e secrétaire gère un ticket
    Le POST contient un parametre ticket
    """
    ok = True
    try:
        ticket = request.POST.get("ticket")
        rr = Reserve_Reparation.objects.get(reparable__ticket = ticket)
        confirmes = Confirme_Reservation.objects.filter(
            reservation = rr).order_by("-confirme_le")
    except:
        ok=False
    data = {
        "html": render_to_string(
            'gestion/secretaire_pour_ticket.html',
            {
                "rr": rr,
                "confirmes": confirmes,
            }),
        "ok": ok,
    }
    return  JsonResponse(data)

def benevole_pour_ticket(request):
    """
    Mise en place du dialogue pour qu'un.e bénévole gère un ticket
    Le POST contient un parametre ticket
    """
    ok = True
    try:
        ticket = request.POST.get("ticket")
        rr = Reserve_Reparation.objects.get(reparable__ticket = ticket)
    except:
        ok=False
    data = {
        "html": render_to_string(
            'gestion/benevole_pour_ticket.html',
            {
                "rr": rr,
            }),
        "ok": ok,
    }
    return  JsonResponse(data)

def secretaire_modif_ticket(request, ticket):
    """
    On arrive là après redirection, quand un.e secrétaire a besoin
    de modifier un ticket, suite à un entretien téléphonique par exemple;
    On propose une interface à peu près semblable à celle qu'utilisent
    les gens pour demander une réparation, on pré-remplit les champs avec
    les valeurs connues.
    """
    rr = Reserve_Reparation.objects.get(reparable__ticket = ticket)
    if "id_event" in request.POST: # on a validé des modifications
        form = ModifReparationForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            rr.reparable.nom = data["nom"]
            rr.reparable.poids = data["poids"]
            rr.reparable.panne = data["panne"]
            rr.reparable.demandeur_nom = data["demandeur_nom"]
            rr.reparable.demandeur_prenom = data["demandeur_prenom"]
            rr.reparable.numero_tel = data["numero_tel"]
            rr.reparable.email = data["email"]
            rr.reparable.save()
            rr.evenement = Evenement.objects.get(id = data["id_event"])
            rr.save()
            return redirect("/tous_tickets#" + rr.reparable.ticket)
    
    form = ModifReparationForm(initial = {
        'nom': rr.reparable.nom,
        'poids': rr.reparable.poids,
        'panne': rr.reparable.panne,
        'demandeur_nom': rr.reparable.demandeur_nom,
        'demandeur_prenom': rr.reparable.demandeur_prenom,
        'numero_tel': rr.reparable.numero_tel or "?",
        'email': rr.reparable.email or '?',
        'evenement': rr.evenement.ou_quand,
        'id_event': rr.evenement.id,
    })
    # autres choix pour l'évènement
    maintenant = timezone.localtime(timezone.now())
    dans_trois_mois = maintenant + timedelta(days=TROIS_MOIS)
    lieux = sorted(list(Repair_Cafe.objects.all()),
               key = lambda rc: rc.ville)
    evenements = collections.OrderedDict(
        (
            (rc.nom_complet, {
                "rc": rc,
                "ev": Evenement.objects.filter(
                    rc = rc,
                    debut__gt = maintenant,
                    debut__lt = dans_trois_mois)
             }) for rc in sorted(list(Repair_Cafe.objects.all()),
                                 key = lambda rc: rc.nom_complet)
        )
    )
    return render(
        request, "gestion/modif_ticket.html", {
            "nom_page" : "Modification de rendez-vous",
            "version":  'RC-web 0.1',
            "rr" : rr,
            'is_secretaire': is_secretaire(request.user),
            'form': form,
            'evenements': evenements,
            'evenement_courant': rr.evenement,
        }
    )

def quel_evenement(request):
    """
    Trouve l'évènement qui correspond à un identifiant. Le POST contient
    le paramètre id. Renvoie un résumé de l'évènement, au format JSON
    """
    id = int(request.POST.get("id"))
    ev = Evenement.objects.get(id = id)
    data = {
        "ok": True,
        "id": id,
        "val": ev.ou_quand,
    }
    return  JsonResponse(data)
    
def reparations_approuvees(request):
    """
    Visualisation des réparations approuvées par des secrétaires.
    le POST peut contenir des paramètres : benevole, ticket, comment
    """
    #if not benevole(request):
    #    return redirect("/")
    if "name" in request.POST: # ça vient d'un.e bénévole
        ticket = request.POST.get("ticket")
        rr = Reserve_Reparation.objects.get(reparable__ticket = ticket)
        comment = request.POST.get("commentaire")
        ticket = request.POST.get("ticket")
        benevole = Benevole.objects.get(user = request.user)
        qr = Qui_Repare(
            benevole = benevole, commentaire = comment, a_reparer = rr)
        qr.save()
    start = timezone.localtime(timezone.now())
    rrs = [rr for rr in Reserve_Reparation.objects.filter(
        evenement__debut__gt = timezone.now()) if rr.confirmations]
    form = AdopteReparationForm()
    return render(
        request, "gestion/reparations_approuvees.html", {
            "nom_page" : "Réparations adoptées",
            "version":  'RC-web 0.1',
            "rrs" : rrs,
            "form": form,
        }
    )

def info_(request):
    """
    fabrique le journal, dans une instance de io.BytesIO(),
    qui est renvoyée avec son curseur au début.
    @return une instance de io.BytesIO()
    """
    result = io.BytesIO()
    if not secretaire(request):
        result.write("Le journal est réservé aux secrétaires.\n".encode("utf-8"))
    else:
        lignes = [str(l).encode("utf-8") for
                  l in RC_Log.objects.all().order_by("-date")]
        result.write(b"\n".join(lignes) + b"\n")
    result.seek(0)
    return result

def journal(request):
    """
    Téléchargement du journal, à un format texte, une ligne par entrée
    """
    d = datetime.today().strftime('%Y-%m-%d_%H:%M:%S')
    return FileResponse(
        info_(request), as_attachment = True, filename = f"Journal_RC_{d}.txt")

def zipfichier(request):
    """
    Téléchargement de la base de données, à un format compressé (ZIP)
    """
    d = datetime.today().strftime('%Y-%m-%d_%H:%M:%S')
    buffer_ = io.BytesIO()
    if secretaire(request):
        with zipfile.ZipFile(buffer_, mode='w') as myzip:
            myzip.write("db.sqlite3")
            filename = f"RC_{d}.zip"
    else:
        buffer_.write("La sauvegarde est réservé aux secrétaires.\n".encode("utf-8"))
        filename = f"RC_{d}.txt"
    buffer_.seek(0)
    return FileResponse(
        buffer_, as_attachment = True, filename = filename)
        
def change_session_lieu(request):
    """
    Modification du lieu dont s'occupe une ou un secrétaire
    afficheur du dialogue
    """
    lieux = Repair_Cafe.objects.all().order_by('nom_court')
    data = {
        "html": render_to_string(
            'gestion/change_session_lieu.html',
            {
                "lieux": lieux,
                "lieu": request.POST.get("lieu")
            }),
    }
    return JsonResponse(data)

def check_session_dates(request, evs = None):
    """
    Application de quelques règles concernant les dates de travail d1 et d2
    @param request la requête, qui sert à accéder aux paramètres de session
    @param ev une liste d'Evenement d'un lieu ou None (si lieu==all)
    @return d1, d2 début et fin pour la ou les dates de travail
    """
    # une date_travail, déjà ?
    d1 = request.session.get("date_travail1", None)
    d2 = request.session.get("date_travail2", None)
    if evs: # il s'agit d'un repair-café, le lieu n'est pas "all"
        futur_evs = evs.filter(debut__gt = datetime.today()).order_by('debut')
        # si la date_travail ne matche pas, on prend une date dans le futur
        if (d1 is None or d1 not in \
            [ev.debut.strftime("%d-%m-%Y") for ev in evs]):
            if futur_evs:
                d1 = futur_evs[0].debut.strftime("%d-%m-%Y")
                request.session["date_travail1"] = d1
            else:
                request.session["date_travail1"] = ""
                # on ne garde la deuxième date que si elle est postérieure à d1
    # en aucun cas, d2 ne peut être antérieure à d1
    if not d1 or not d2 or datetime.strptime(d1, "%d-%m-%Y") > \
       datetime.strptime(d2, "%d-%m-%Y"):
        # d2 n'est pas postérieure à d1
        d2 = None
        request.session["date_travail2"] = ""
    if d1 and d2 and d2 == d1:
        # si d1 et d2 sont la mêmedate, on supprime d2
        d2 = None
        request.session["date_travail2"] = ""
    return d1, d2
 
def change_session_lieu_go(request):
    """
    Modification du lieu dont s'occupe une ou un secrétaire
    rafraîchissement de la page
    """
    id_lieu = request.POST.get("lieu")
    if not id_lieu:
        return JsonResponse({"OK": "false"})
    if id_lieu != "all":
        id_lieu = Repair_Cafe.objects.get(id=id_lieu).nom_court
        evs = Evenement.objects.filter(rc__nom_court = id_lieu)
        d1, d2 =  check_session_dates(request, evs)
    else: # id_lieu est "all"
        d1, d2 =  check_session_dates(request, None)
        
    request.session["lieu"] = id_lieu
    data = {
        "OK": "true",
    }
    return JsonResponse(data)

def change_session_date_travail(request):
    """
    Modification de la date dont s'occupe une ou un secrétaire
    afficheur du dialogue. Le POST contient les paramètres
    lieu, date1 et date2
    """
    lieu = request.POST.get("lieu")
    date1 = request.POST.get("date1","")
    date2 = request.POST.get("date2","")
    data = {
        "html": render_to_string(
            'gestion/change_session_date_travail.html',
            {
                "lieu": lieu,
                "date1": date1,
                "date2": date2,
            }),
    }
    return JsonResponse(data)

def change_session_date_travail_go(request):
    """
    Modification de la date dont s'occupe une ou un secrétaire
    afficheur du dialogueprocédure d'inscription dans les paramètres
    de session. Le POST contient les paramètres date1 et date2
    """
    date1 = request.POST.get("date1","")
    date2 = request.POST.get("date2","")
    lieu = request.session.get("lieu", "")
    request.session["date_travail1"] = date1
    request.session["date_travail2"] = date2
    ok = "true"
    if not lieu:
        ok="false"
    elif lieu == "all":
        d1, d2 =  check_session_dates(request, None)
    else:
        evs = Evenement.objects.filter(rc__nom_court = lieu)
        d1, d2 =  check_session_dates(request, evs)
    data = {
        "OK": ok,
    }
    return JsonResponse(data)

def categories(request):
    categories = Categorie.objects.order_by('sans_accent')
    return render(
        request,
        'gestion/categories.html',
        {
            "nom_page": 'Liste des catégories',
            "version":  'RC-web 0.1',
            "is_secretaire": secretaire(request),
            "categories": categories,
        }
    )

def nouvelle_categorie(request):
    """
    Création d'une nouvelle catégorie, POST contient un paramètre 'nom'
    """
    nom = request.POST.get("nom")
    trouves = Categorie.objects.filter(nom = nom)
    ok = "true" if not trouves else "false"
    if not trouves:
        new = Categorie(nom = nom)
        new.save()
    data = {
        "OK": ok,
    }
    return JsonResponse(data)

def edite_categorie(request):
    """
    Modification d'une catégorie
    le POST contient un paramètre 'categorie'
    """
    if "id_categorie" in request.POST:
        # ça vient de la page '/categorie'
        c = Categorie.objects.get(pk = request.POST.get("id_categorie"))
        form = categorie_Form(instance=c)
    else:
        c = Categorie.objects.get(nom = request.POST.get("nom"))
        form = categorie_Form(request.POST, request.FILES, instance = c)
        if form.is_valid() and "validation" in request.POST:
            # on n'enregistre que si la donnée vient d'un formulaire validé
            form.save()
            log(c, action="write")
            return redirect("/categories")
    return render(
        request,
        'gestion/edite_categorie.html',
        {
            "nom_page": 'modification d\'une catégorie',
            "version":  'RC-web 0.1',
            "is_secretaire": secretaire(request),
            "form": form,
            "nom": c.nom,
        }
    )
    
def supprime_categorie(request):
    """
    Supprime une catégorie ; le POST a un paramètre "nom"
    """
    nom =request.POST.get("nom")
    categorie = Categorie.objects.get(nom = nom)
    categorie.delete()
    data = {
        "OK": "true",
    }
    return JsonResponse(data)

def lieu_plus_categorie(request):
    """
    associe un lieu et une catégorie.
    Le POST contient des paramètres lieu_id et categorie_id
    """
    l = Repair_Cafe.objects.get(pk = request.POST.get("lieu_id"))
    c = Categorie.objects.get(pk = request.POST.get("categorie_id"))
    lc, created = LC.objects.get_or_create(lieu = l, categorie = c)
    if created:
        lc.save()
    data = {
        "OK": "true",
    }
    return JsonResponse(data)

   
def lieu_moins_categorie(request):
    """
    désassocie un lieu et une catégorie.
    Le POST contient des paramètres lieu_id et categorie_id
    """
    l = Repair_Cafe.objects.get(pk = request.POST.get("lieu_id"))
    c = Categorie.objects.get(pk = request.POST.get("categorie_id"))
    lcs = LC.objects.filter(lieu = l, categorie = c)
    lcs.delete()
    data = {
        "OK": "true",
    }
    return JsonResponse(data)

   
def exporte_tableur(request, id):
    # i18n
    from gettext import gettext as _
    package = get_packagename()
    localize(get_packagename())

    if id: # on a demandé un rapport
        rc = Repair_Cafe.objects.get(pk = id)
        categories = [lc.categorie.nom for lc in LC.objects.filter(lieu = rc)]
        infile = str(MEDIA_ROOT) + "/docs/modele0.ods"
        fname = datetime.now().strftime("rapport_%Y-%m-%d_%H:%M.ods")
        outfile = str(MEDIA_ROOT) + "/docs/" + fname
        makeSheet(
        verbose = True,
            infile = infile, outfile = outfile,
            rc = rc.nom_court,
            date = datetime.now().strftime("le %d/%m/%Y"),
            categories = categories
        )
        return FileResponse(
            open(outfile, 'rb'), as_attachment=True, filename= fname
        )
        
    lieux = Repair_Cafe.objects.order_by('nom_court')
    for l in lieux:
        l.categories = [lc.categorie for lc in LC.objects.filter(lieu = l)]
    
    
    return render(
        request,
        'gestion/exporte_tableur.html',
        {
            "nom_page": 'export des données dans un tableur',
            "version":  'RC-web 0.1',
            "is_secretaire": secretaire(request),
            "lieux": lieux,
        }
    )
